﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pendulum : MonoBehaviour
{
    private Rigidbody _rigidbody;
    private float _lastX;
    public float TargetVelocity;
    //public float Force;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (_rigidbody.angularVelocity.x < TargetVelocity)
        {
            if (_lastX <= transform.position.x)
                _rigidbody.AddForce(Vector3.right * TargetVelocity, ForceMode.Acceleration);
            else
                _rigidbody.AddForce(Vector3.left * TargetVelocity, ForceMode.Acceleration);
        }
        //if (_rigidbody.angularVelocity.x > TargetVelocity)
        //{
        //    if (_lastX < transform.position.x)
        //        _rigidbody.AddForce(Vector3.left * Force, ForceMode.Acceleration);
        //    else
        //        _rigidbody.AddForce(Vector3.right * Force, ForceMode.Acceleration);
        //}
        _lastX = transform.position.x;
    }
}
