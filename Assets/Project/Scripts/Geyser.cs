﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Geyser : MonoBehaviour
{
    public float Interval;
    public float EruptionTime;
    public ParticleSystem Particle;

    private Collider _collider;
   
    void Awake()
    {
        _collider = GetComponent<Collider>();
    }

    void Start()
    {
        StartCoroutine(EruptionCycle());
    }

    private IEnumerator EruptionCycle()
    {
        while (true)
        {
            yield return new WaitForSeconds(Interval);

            //Debug.Log("Eruption start");
           
            Particle.Play();
            yield return new WaitForSeconds(1);
            _collider.enabled = true;
            yield return new WaitForSeconds(EruptionTime);

            _collider.enabled = false;
            Particle.Stop();

            //Debug.Log("Eruption end");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Destroy " + other.name);
            other.GetComponent<PlayerScript>().LoseLife();
        }
        if (other.CompareTag("Boss"))
        {
            Debug.Log("Boss Hit");
            other.GetComponent<Boss>().GeyserHit();
        }
    }
}
