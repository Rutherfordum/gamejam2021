﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Globefish : MonoBehaviour
{
    public float MaxScale;
    public float ScaleStep;
    public float ScaleSpeed;
    public float Interval;
    public float BigFormTime;

    private Collider _collider;
    private float _startScale;

    
    void Awake()
    {
        _collider = GetComponent<Collider>();
        _startScale = transform.localScale.x;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.GetComponent<PlayerScript>().LoseLife();
        }
    }

    void Start()
    {
        StartCoroutine(GlobefishCycle());
    }

    private IEnumerator GlobefishCycle()
    {
        while (true)
        {
            yield return new WaitForSeconds(Interval);

            while(transform.localScale.x < MaxScale)
            {
                transform.localScale = new Vector3(
                    transform.localScale.x + ScaleStep,
                    transform.localScale.y + ScaleStep,
                    transform.localScale.z);

                yield return new WaitForSeconds(ScaleSpeed);
            }
            
            _collider.enabled = true;

            yield return new WaitForSeconds(BigFormTime);

            _collider.enabled = false;

            while (transform.localScale.x > _startScale)
            {
                transform.localScale = new Vector3(
                    transform.localScale.x - ScaleStep,
                    transform.localScale.y - ScaleStep,
                    transform.localScale.z);

                yield return new WaitForSeconds(ScaleSpeed);
            }
        }
    }
}
