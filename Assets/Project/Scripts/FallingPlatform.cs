﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FallingPlatform : MonoBehaviour
{
    public float MaxHeightShift;
    public float MaxSpeed;
    public float Acceleration;
    public bool UseNormalsOnCollision;

    private Rigidbody _rigidbody;
    private float _startY;
    private bool _playerOnPlatform;

    void Start()
    {
        _startY = transform.position.y;
        _rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (_startY - transform.position.y < MaxHeightShift
            && _rigidbody.velocity.y < MaxSpeed
            && _playerOnPlatform)
        {
            _rigidbody.AddForce(Vector3.down * Acceleration, ForceMode.Acceleration);
        }
        else if (transform.position.y < _startY
            && _rigidbody.velocity.y < MaxSpeed
            && !_playerOnPlatform)
        {
            _rigidbody.AddForce(Vector3.up * Acceleration, ForceMode.Acceleration);
        }
        else if (transform.position.y >= _startY)
        {
            _rigidbody.velocity = Vector3.zero;
            transform.position = new Vector3(transform.position.x, _startY, transform.position.z);
        }
        else if (transform.position.y <= _startY - MaxHeightShift)
        {
            _rigidbody.velocity = Vector3.zero;
            transform.position = new Vector3(transform.position.x, _startY - MaxHeightShift, transform.position.z);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (!UseNormalsOnCollision)
            {
                _playerOnPlatform = true;
                //Debug.Log("_playerOnPlatform");
            }
            else
            {
                //// Print how many points are colliding with this transform
                //Debug.Log("Points colliding: " + other.contacts.Length);

                //// Print the normal of the first point in the collision.
                //Debug.Log("Normal of the first point: " + other.contacts[0].normal);

                //// Draw a different colored ray for every normal in the collision
                //foreach (var item in other.contacts)
                //{
                //    Debug.DrawRay(item.point, item.normal * 100, Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f), 10f);
                //}

                var contacts = new List<ContactPoint>(other.contactCount);
                other.GetContacts(contacts);
                if (contacts.Any(c => c.normal == Vector3.down))
                {
                    _playerOnPlatform = true;
                    //Debug.Log("_playerOnPlatform");
                }
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            _playerOnPlatform = false;
            //Debug.Log("_playerOutPlatform");
        }
    }
}
