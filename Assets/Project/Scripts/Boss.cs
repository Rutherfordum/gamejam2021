﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public GameObject Korona;
    public Animator anim;
    public short CurrentHP;

    public short DamagePerGeyser;

    public Rigidbody Bullet;
    public float BulletSpeed;
    public float BulletLifetime;
    public float BulletStartPointShift;
    public float ShootAngle;
    public float ShootDelay;

    public bool MoveHorizontally;
    public bool MoveVertically;
    public float HorizontalRange;
    public float VerticalRange;
    public float HorizontalSpeed;
    public float VerticalSpeed;

    private float _lastShootTime;
    private float _startX;
    private bool _moveLeft;
    private float _startY;
    private bool _moveDown;

    public void GeyserHit()
    {
        CurrentHP -= DamagePerGeyser;
        anim.Play("atack");
        if (CurrentHP <= 0)
        {
            GetComponent<Boss>().enabled = false;
            GetComponent<Rigidbody>().isKinematic = false;
            anim.Play("deadboss");
            Korona.SetActive(true);
        }
        
    }

    void Start()
    {
        _startX = transform.position.x;
        _startY = transform.position.y;
        _moveLeft = true;
        _moveDown = true;
    }

    void FixedUpdate()
    {
        Shoot();
    }

    void Update()
    {
        if (MoveHorizontally)
            HorizontalMovement();
        if (MoveVertically)
            VerticalMovement();
    }

    private void Shoot()
    {
        if (Time.fixedTime - _lastShootTime >= ShootDelay)
        {
            var currentVector = transform.right;
            var sumAngle = 0f;
            while (sumAngle < 360)
            {
                SingleShoot(currentVector);
                currentVector = Quaternion.AngleAxis(-ShootAngle, Vector3.forward) * currentVector;
                sumAngle += ShootAngle;
            }

            _lastShootTime = Time.fixedTime;
        }
    }

    private void SingleShoot(Vector3 angle)
    {
        var clone = Instantiate(Bullet, transform.position + angle * BulletStartPointShift, Quaternion.identity);
        clone.gameObject.SetActive(true);
        clone.AddForce(angle * BulletSpeed, ForceMode.VelocityChange);
        Destroy(clone.gameObject, 10);
    }

    private void HorizontalMovement()
    {
        var moveDistance = HorizontalSpeed * Time.deltaTime;
        var currentX = transform.position.x;
        if (_moveLeft)
        {
            if (currentX - moveDistance <= _startX - HorizontalRange)
            {
                transform.position = new Vector3(_startX - HorizontalRange, transform.position.y, transform.position.z);
                _moveLeft = false;
            }
            else
                transform.position -= new Vector3(moveDistance, 0, 0);
        }
        else if (!_moveLeft)
        {
            if (currentX + moveDistance >= _startX)
            {
                transform.position = new Vector3(_startX, transform.position.y, transform.position.z);
                _moveLeft = true;
            }
            else
                transform.position += new Vector3(moveDistance, 0, 0);
        }
    }

    private void VerticalMovement()
    {
        var moveDistance = VerticalSpeed * Time.deltaTime;
        var currentY = transform.position.y;
        if (_moveDown)
        {
            if (currentY - moveDistance <= _startY - VerticalRange)
            {
                transform.position = new Vector3(transform.position.x, _startY - VerticalRange, transform.position.z);
                _moveDown = false;
            }
            else
                transform.position -= new Vector3(0, moveDistance, 0);
        }
        else if (!_moveDown)
        {
            if (currentY + moveDistance >= _startY)
            {
                transform.position = new Vector3(transform.position.x, _startY, transform.position.z);
                _moveDown = true;
            }
            else
                transform.position += new Vector3(0, moveDistance, 0);
        }
    }
}
