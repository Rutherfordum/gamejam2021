﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularSaw : MonoBehaviour
{
    public float Range;
    public float Speed;
    public GameObject SR;

    private float _startX;
    private bool _moveRight;

    

    void Start()
    {
        _startX = transform.position.x;
        _moveRight = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.GetComponent<PlayerScript>().LoseLife();
        }
    }

    void Update()
    {
        var moveDistance = Speed * Time.deltaTime;
        var currentX = transform.position.x;
        if (_moveRight)
        {
            if (currentX + moveDistance >= _startX + Range)
            {
                transform.position = new Vector3(_startX + Range, transform.position.y, transform.position.z);
                _moveRight = false;
                SR.GetComponent<SpriteRenderer>().flipX = false;
            }
            else
                transform.position += new Vector3(moveDistance, 0, 0);
        }
        else if (!_moveRight)
        {
            if (currentX - moveDistance <= _startX)
            {
                transform.position = new Vector3(_startX, transform.position.y, transform.position.z);
                _moveRight = true;
                SR.GetComponent<SpriteRenderer>().flipX = true;
            }
            else
                transform.position -= new Vector3(moveDistance, 0, 0);
        }
    }
}
