﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public Text helth;
    public Animator anim;
    public int LifesCount;

    void Start()
    {
        helth.text = "У тебя осталось " + LifesCount.ToString() + " жизней";


    }

    public void LoseLife()
    {
        LifesCount--;
        if (LifesCount == 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        helth.text = "У тебя осталось " + LifesCount.ToString() + " жизней";
        anim.Play("Dead");
    }

    void Update()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }
}
