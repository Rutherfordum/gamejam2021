﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switcher : MonoBehaviour
{
    public bool Value;

    public GameObject gazer;

    private bool _playerInside;
    public GameObject anchor;
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _playerInside = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _playerInside = false;
        }
    }

    void Update()
    {        
        if (_playerInside && Input.GetKeyDown(KeyCode.E))
        {
            Value = !Value;
            anchor.transform.eulerAngles = Value ? new Vector3(0, 0, 45) : new Vector3(0, 0, -45);
            gazer.SetActive(Value);
            //Debug.Log("Value changed to " + Value);
        }
    }
}
