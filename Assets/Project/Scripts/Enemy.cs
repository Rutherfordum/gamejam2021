﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float DetectDistance;
    public Rigidbody Bullet;
    public float BulletSpeed;
    public float BulletLifetime;
    public float ShootDelay;
    public float BulletPointShiftX;
    public float BulletPointShiftY;

    public bool IsMoving;
    public float MoveRange;
    public float MoveSpeed;

    private float _startX;

    private bool _playerDetected;
    private float _lastShootTime;

    void Start()
    {
        if (IsMoving)
        {
            _startX = transform.position.x;
        }
    }

    void FixedUpdate()
    {
        var detected = Physics.Raycast(transform.position, transform.right, DetectDistance, 1 << 31);
        if (detected)
        {
            _playerDetected = true;
            Shoot();
            //Debug.Log("detected");
        }
        else
        {
            _playerDetected = false;
        }
    }

    void Update()
    {
        if (IsMoving && !_playerDetected)
            Move();
    }

    private void Shoot()
    {
        if (Time.fixedTime - _lastShootTime >= ShootDelay)
        {
            var bulletPosition = new Vector3(transform.position.x + BulletPointShiftX * transform.right.x,
                transform.position.y + BulletPointShiftY, transform.position.z);
            var clone = Instantiate(Bullet, bulletPosition, Quaternion.identity);
            clone.gameObject.SetActive(true);
            clone.AddForce(transform.right * BulletSpeed, ForceMode.VelocityChange);
            Destroy(clone.gameObject, 10);
            _lastShootTime = Time.fixedTime;
        }
    }

    private void Move()
    {
        var moveDistance = MoveSpeed * Time.deltaTime;
        var currentX = transform.position.x;
        var destination = transform.position + transform.right * moveDistance;
        if (Mathf.Abs(destination.x - _startX) >= MoveRange)
        {
            destination = new Vector3(_startX + transform.right.x * MoveRange,
                transform.position.y, transform.position.z);
            transform.position = destination;
            transform.Rotate(new Vector3(0, 180, 0));
            //Debug.Log("1");
        }
        else if (IsBetweenRange(_startX, transform.position.x, destination.x) 
            && _startX != transform.position.x)
        {
            destination = new Vector3(_startX,
               transform.position.y, transform.position.z);
            transform.position = destination;
            transform.Rotate(new Vector3(0, 180, 0));
            //Debug.Log("2");
        }
        else
        {
            transform.position = destination;
        }       
    }

    public bool IsBetweenRange(float thisValue, float value1, float value2)
    {
        return thisValue >= Mathf.Min(value1, value2) && thisValue <= Mathf.Max(value1, value2);
    }
}
